#!/bin/bash
zeronet_data_dir="/srv/zeronet/data/"
knowledge_dir="$HOME/ZeroNet/data/1SearchPd3khzLtsxTxKYhYUohk7c1QYd/knowledge"
cd $zeronet_data_dir || exit
grep -hinora http://127.0.0.1:43110/1................................. | \
  sed -e 's/^[0-9]*://;s/"$//;s/\\$//;s/)$//;s/\/$//;s/\]$//' | sort | uniq > \
  "$knowledge_dir/address_list.txt"
