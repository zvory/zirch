#!/bin/bash
zeronet_dir=$HOME/ZeroNet/data/1SearchPd3khzLtsxTxKYhYUohk7c1QYd/
cd "$zeronet_dir" || exit
echo "gathering addresses"
bash addressGather.sh
echo "gathering bit addresses"
bash bitAddressGather.sh
echo "loading all"
bash allLoad.sh
echo "listing used ones"
bash listUsed.sh
echo "generating searchJsonGen"
node searchJsonGen.js
echo "knowledgeGathering"
bash knowledgeGather.sh
